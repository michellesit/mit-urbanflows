# UrbanFlows #

Written by Michelle Sit

Server-client network. Server passes paramters to SlavePi via TCP connection. Client should run without Server connection. Works under lab conditions and if connection to Server over wifi is dropped. There is one error: under less than ideal wifi conditions, the ssh connection may break between the server and client. This will cause the system to stop recording. I am not certain what the error is.


#slavePi files:#

###Basic raspi files###
*   preview.py - Shows camera image for a specified time.  Doesn't store the images.   
*   removePics.sh - Removes all .jpg and .h264 (video) files from the current Pi   
*   restartWifi.sh - Restarts wifi if wifi is down and Pi is connected by ethernet   
*   takePic.py - Takes a picture.  Can be used to check if the camera is in use.

###Client files###
*	testUploadSpeed4.py - Main client file. Need to change server save file path to yours and internal checks for serverIP Address
*	manualPic_capturePhotos3.py - takes pictures based on inputs and uploads them to a server
*	videoMode4.py - takes video based on inputs and uploads them to a server

###CRON/Maintenance files###
*	emailIPAddr.sh - Add this file to your CRONTAB. It can be used to send the slavePi's IP address to an email if the IP address changes. It can also be used to monitor the status of your wifi at the location (ie if no emails with IP address, then slavePi is down)
*	checkWifi.py - Add this file to your CRONTAB. It will check the wifi connection and attempt to handle the issue and restart the connection three times. Writes output to wifiLog.txt.

*	uploadScript.py - not used for our system, but I sent this file to Philips earlier. Grabs all of the videos and sends them to the file server. You will need to specify the server save path.


###Bluetooth files###
*	bluetoothClient.py - Use this to connect to another bluetoothServer.py using their bluetooth MAC address. To run: python bluetoothClient.py (MAC ADDRESS)
*	bluetoothServer.py - Add this file to your rc.local and have it run in the background. File will allow full access to any bluetooth user with the password.


###Server files:###
*	callbackServer6.py - most up to date server. Server will not send the start record trigger until all of the slavePis have initially connected to the server. If the connection between the slavePies and Server breaks later in the process, the server continues running.
*	masterVariables2.py - helper config file

#Additional libraries to install#

###Set up email:###
*	https://www.cyberciti.biz/tips/linux-use-gmail-as-a-smarthost.html
*	sudo apt-get install ssmtp mailutils
*	Make sure to configure ssmtp.conf file

###Install sshpass:###
*	sudo apt-get install sshpass
*	scp a file to the Beast first. Have them shake hands so they connect.

###Install mjpeg-streamer:###
*	https://blog.miguelgrinberg.com/post/how-to-build-and-run-mjpg-streamer-on-the-raspberry-pi 

###Set up Twisted:###
* sudo apt-get install python-twisted

###Setting up bluetooth:###
* **Pybluez github:** https://github.com/karulis/pybluez 

###Instructions for making raspies discoverable:###
* http://raspberrypi.stackexchange.com/questions/7236/what-commands-do-i-use-to-make-bluetooth-discoverable-on-rpi-running-raspbian
*Modify rc.local to make bluetooth discoverable on tart

#Run instructions#

###For Client SlavePi:###
*	Add emailIPAddr.sh and checkWifi.py to your CRONTAB. Add bluetoothServer.py (run in background) and email IP address on start (emailIPAddr.sh) to rc.local
*	Check the SlavePi Bluetooth MAC address and save it somewhere for later
*	To connect to Server to start: python testUploadSpeed4.py (Your Pi's name)

###For Server SlavePi:###
*	Add all of the recording dates and times in callbackServer6.py. The server will ask for the first date and time in the prompt
*	To start the Server: python callbackServer6.py