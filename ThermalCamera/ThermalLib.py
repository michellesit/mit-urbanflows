#!/usr/bin/python


import struct
from collections import namedtuple
import numpy as np
import cv2
import time

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as md

img_header = namedtuple('ihead', 'time, ffc_counter, fpa_temperature')
header_struct = namedtuple('header', 'time, gps_status, acc_x, acc_y, acc_z, mag_x, mag_y, mag_z')
gps_struct = namedtuple('gps', 'time, mode, ept, latitude, epy, longitude, epx, altitude, epv, track, epd, speed, eps, climb, epc')

def image_generator(file_path, skip_frames=0, record_len = 9608):
    file = open(file_path, 'rb')
    file.seek(record_len * skip_frames)
    head_len = record_len - 9600
    head_pad = bytes([0] * (8 - head_len))

    while 1:
        content = file.read(record_len)
        if len(content) < record_len:
            break
        
        _header = img_header._make(struct.unpack("ihh", content[:head_len] + head_pad))
        lepton_data = struct.unpack("H" * 4800, content[head_len:])
        _data = np.asarray(lepton_data, dtype=np.uint32).reshape(60, 80, 1)   
        yield _data, _header


def sensor_generator(file_path):
    file = open(file_path, 'rb')

    while 1:
        content = file.read(18)
        if (len(content) != 18):
            break

        header = header_struct._make(struct.unpack('ihhhhhhh', content))
        gps = None
        if header.gps_status == 1:
            gps_content = file.read(120)
            if (len(gps_content) == 120):
                gps = gps_struct._make(struct.unpack('diddddddddddddd', gps_content))
                if np.isnan(gps.latitude) or np.isnan(gps.longitude):
                    continue
    
        yield header, gps        


def thermal_image(data, colorMap = 2, resizeScale = 5, colorRange = [0, 65535], rotate = 0, segments = None):
    #capSize = (80 * scale, 60 * scale) if (rotate is 0 or rotate is -2) else (60 * scale, 80 * scale)
    if rotate is not 0:
        data = np.rot90(data,rotate)
        data = data.copy()

    data = np.uint16(data)      
    cv2.normalize(data, data,  0,65535, cv2.NORM_MINMAX)
    np.right_shift(data, 8, data)

    if colorMap < 0: # image requested for video
        img = cv2.applyColorMap(np.uint8(data), -colorMap)
    else:
        img = cv2.applyColorMap(np.uint8(data), colorMap)
        img[:,:,[2,0]] = img[:,:,[0,2]]
    
    img = cv2.resize(img, (0,0), fx=resizeScale, fy=resizeScale) 

    if segments:
        polys = []
        for segment in segments:
            segment = [(b, a) for a, b in segment]
            poly = np.array(segment) * resizeScale
            cv2.polylines(img, [poly], 1, (255,255,255))

    return img


# rotate = -1 => rotate -90 , rotate = 1 => rotate 90
def thermal_video(image_generator, video_file, segments = None, mse_treshold = 0, color_map = 2, scale = 5, time_shift = 0, rotate = 0):
    capSize = (80 * scale, 60 * scale) if (rotate is 0) else (60 * scale, 80 * scale)
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v') 
    video = cv2.VideoWriter()
    success = video.open(video_file, fourcc, 15, capSize,True) 

    prev_data = [[0] * 4800]
    
    frame = 0
    for data, header in image_generator:
        if (mse_treshold > 0):
            mse = pow(data-prev_data,  2).sum() / 4800
            prev_data = data
            if mse < mse_treshold:
                continue
        
        capture_time = header[0]

        if rotate is not 0:
            data = np.rot90(data,rotate)
            data = data.copy()
            
        img = thermal_image(data, colorMap = -color_map, resizeScale = scale, segments=segments)

        cv2.putText(img, time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(capture_time + time_shift)), (5, 35),cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255,255,255))
        cv2.putText(img, str(frame), (5, 265),cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0,0,0))
        video.write(img)
        frame+=1 
    video.release() 


def makeSegment(x,y,height,width):
    return [(x,y), (x,y+width), (x+height, y+width), (x+height, y) ] 


""" determine the point inside a given polygon 

Args: 
    Polygon as a list of (x,y) pairs

Returns:
    masked array for filtering points of raw data matrix

"""
def inside_points(points):
    mask = np.ones((60,80))
    for x in range(0,60):
        for y in range(0,80):
            n = len(points)
            inside = False
            p1x, p1y = points[0]
            for i in range(1, n + 1):
                p2x, p2y = points[i % n]
                if y > min(p1y, p2y):
                    if y <= max(p1y, p2y):
                        if x <= max(p1x, p2x):
                            if p1y != p2y:
                                xinters = (y - p1y) * (p2x - p1x) / (p2y - p1y) + p1x
                            if p1x == p2x or x <= xinters:
                                inside = not inside
                p1x, p1y = p2x, p2y
            if (inside == True):
                mask[x,y] = 0
    return mask


""" returns the radon sinogram for 0 or 90 degree values

Args: 
    thermal image data array
    segment 
    mapping degree: 0 is top-down, 1 is left-right

Returns:
    sinogram in [0..100] range for the given segment 

"""
def axis_radon(data, segment=[(0,0),(0,80),(60,80),(60, 0)], axis = 0):
    sdata = data[segment[0][0]:segment[2][0],segment[0][1]:segment[2][1]]
    mapped = sdata.sum(axis)
    
    min_val = mapped.min()
    max_val = mapped.max()
    normal = (mapped - min_val) * 100 / (max_val - min_val)
    return normal.flatten().round()

""" removes the colder areas of the data

Args: 
    thermal image data array
    replaceZeros specifies if cut part is replaces with zeros or the cut-point value

Returns:
    data array with colder areas replaced by the cut-point value

"""
def hist_cut(data, replaceZeros = False):
    hist,bins = np.histogram(data,bins = range(7000, 9000, 20))
    ind = np.argmax(hist)
    cut = bins[ind+1]
    if replaceZeros:
        data[data < cut] = 0
    else:
        data[data < cut] = cut
    
    return data

def print_time(t):
    print (time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(t)))
    return