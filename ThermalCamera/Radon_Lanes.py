
# coding: utf-8

# In[3]:

import sys
import time
import struct
import numpy as np
import cv2

import matplotlib.pyplot as plt

sys.path.append('../lib')
import ThermalLib as lep

get_ipython().magic('matplotlib inline')

import csv
from skimage.transform import radon

from collections import namedtuple
from itertools import count
import scipy.stats as stats


# In[11]:

def box1cam1():
    for data,header in lep.image_generator("/home/sam/diskdump/thermal/cam1s.bin"):
        data = meanpass(data)
        yield {'img':data.squeeze(2), 'time': header.time}

def box1cam0():
    for data,header in lep.image_generator("/home/sam/diskdump/thermal/cam0s.bin"):
        data = data[:,10:]
        yield {'img':data.squeeze(2), 'time': header.time}


# In[1243]:

def iterlen(g):
    for _, i in zip(g, count(1)): pass
    return i


# In[560]:

rc = radon_cars(remove_duplicate_frames(box1cam1()))


# In[228]:

def in_sample(f):
    for i, a in enumerate(f):
        if i not in box1cam1_samples: continue
        yield a


# In[4]:

def radon_cars(f):
    for d in f:
        img = d['img']
        img = img.astype(np.float)
        lanes = img.sum(1)
        lane_peaks = peaks(lanes, thresh=0.1, mpd=15)
        num_cars = 0
        for l in lane_peaks:
            lane_img = img[max(l-5,0):l+5]
            cars = lane_img.sum(0)
            wheels = peaks(cars, mpd=10)
            num_cars += len(wheels) / 2
        d['cars'] = num_cars
        yield d


# In[202]:

def background_subtract_cars(f):
    fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=False)
    
    params = cv2.SimpleBlobDetector_Params()
    params.filterByColor = False
    params.filterByCircularity = False
    params.filterByConvexity = False
    params.filterByInertia = False
    params.filterByArea = True
    params.minDistBetweenBlobs = 16
    detector = cv2.SimpleBlobDetector_create(params)
    fat = np.ones((2,4), np.uint8)
    kernel = np.ones((2,4), np.uint8)
    
    for i,d in enumerate(f):
        img = d['img']
        img = img.astype(np.float32)
        img -= img.min()
        img /= img.max()
        img *= 255
        img = img.astype(np.uint8)
        fgmask = fgbg.apply(img)
        if i < 100: continue
        if i not in box1cam1_samples: continue # remove this when not
        #  fgmask = cv2.morphologyEx(fgmask, cv2.MORPH_OPEN, kernel)
        fgmask = cv2.dilate(fgmask, fat)
        fgmask = cv2.erode(fgmask, kernel)
        keypoints = detector.detect(fgmask)
        if keypoints:
            colored = np.zeros([*img.shape,3], dtype=np.uint8)
            colored[:,:,1] = fgmask
            im_with_keypoints = cv2.drawKeypoints(colored, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
            d['cars'] = len(keypoints)
            d['img'] = img
            d['diagnostic'] = im_with_keypoints
            yield d


# In[1405]:

def accuracy(counts):
    return sum(d['cars'] == c for (d,c) in zip(cars(box1cam1()), counts)) / len(counts)


# In[9]:

def meanpass(a):
    std = np.std(a)
    mean = np.mean(a)
    a[a > mean + std] = mean + std
    a[a < mean - std] = mean - std
    return a

def modepass(a):
    bins, edges = np.histogram(a.reshape(-1), range(7000, 9000, 10))
    binned_mode = edges[np.argmax(bins)+1]
    a[a < binned_mode] = binned_mode
    return a


# In[58]:

def remove_duplicate_frames(a, mse=80):
    old_frame = None
    for d in a:
        x = d['img']
        if old_frame is not None:
            if np.square(old_frame - x).mean() < mse:
                continue
        yield d
        old_frame = x


# In[11]:

def take(a, n):
    for i in range(n):
        yield next(a)


# In[567]:

def showRadon(data, res_deg = 90):
    rays_no = max(data.shape)
    theta = np.linspace(0., 180., rays_no, endpoint=False)
    sinogram = radon(data, theta= theta )  
    
    plt.imshow(sinogram, cmap=plt.cm.Reds,extent=(0, 180, 0, sinogram.shape[0]), aspect='auto')
    plt.xlabel("Projection angle (deg)")
    plt.ylabel("Projection position (pixels)")
    plt.title("Radon Transform")


# In[1209]:

with open("/home/sam/box1cam1-counts", 'w') as f:
    for c in counts:
        print(c, file=f)


# In[45]:

def plot_peaks(x, indices):
    plt.plot(x, 'b')
    plt.plot(indices, x[indices], 'r+')
    plt.show()


# In[241]:

def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


# In[7]:

# A hill is the highest local maxmimum within a specified x distance, if one exists
# A valley is the smallest local minimum between two hills
# A peak is a hill surrounded by valleys of valley_height < THRESHOLD * peak_height
# Data is padded with the global min on either side so peaks can occur on the edges
def peaks(shifted_xs, mpd=None, thresh=0.16):
    if mpd is None: mpd = shifted_xs.size // 4
    xs = (shifted_xs - shifted_xs.min()).astype(np.float)
    xs = np.pad(xs, 1, 'constant')
    dx = np.diff(xs)
    increasing = dx > 0
    decreasing = dx <= 0
    maxima = increasing[:-1] & decreasing[1:]
    xv = xs[1:]
    ind = np.flatnonzero(maxima)
    ind = ind[np.argsort(xv[ind])][::-1]
    idel = np.zeros(ind.size, dtype=bool)
    for i in range(ind.size):
        if not idel[i]:
            idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd)
            idel[i] = 0
    ind = np.sort(ind[~idel])
    idel = np.zeros(ind.size, dtype=bool)
    prev_valley = 0
    height_diff = thresh * xv.max()
    for i in range(ind.size):
        val = xv[ind[i]] - height_diff
        idel[i] = prev_valley > val
        if i < ind.size - 1:
            prev_valley = xv[ind[i]+1:ind[i+1]].min()
            idel[i] |= prev_valley > val
    return ind[~idel]


# In[32]:

def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, ratio=None, valley=False, show=False, ax=None):


    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    
    if ratio is not None:
        m = np.min(x)
        x = np.pad(x, 1, 'constant', constant_values=m)
        mph = ratio * (np.max(x) - m) + m
        print("MPH", mph)
        
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd)                     & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        _plot(x, mph, mpd, threshold, edge, valley, ax, ind)

    return ind

def _plot(x, mph, mpd, threshold, edge, valley, ax, ind):
    """Plot results of the detect_peaks function, see its help."""
    try:
        import matplotlib.pyplot as plt
    except ImportError:
        print('matplotlib is not available.')
    else:
        if ax is None:
            _, ax = plt.subplots(1, 1, figsize=(8, 4))

        ax.plot(x, 'b', lw=1)
        if ind.size:
            label = 'valley' if valley else 'peak'
            label = label + 's' if ind.size > 1 else label
            ax.plot(ind, x[ind], '+', mfc=None, mec='r', mew=2, ms=8,
                    label='%d %s' % (ind.size, label))
            ax.legend(loc='best', framealpha=.5, numpoints=1)
        ax.set_xlim(-.02*x.size, x.size*1.02-1)
        ymin, ymax = x[np.isfinite(x)].min(), x[np.isfinite(x)].max()
        yrange = ymax - ymin if ymax > ymin else 1
        ax.set_ylim(ymin - 0.1*yrange, ymax + 0.1*yrange)
        ax.set_xlabel('Radon Data', fontsize=14)
        ax.set_ylabel('Heat Intensity', fontsize=14)
        mode = 'Valley detection' if valley else 'Peak detection'
        #ax.set_title("%s (mph=%s, mpd=%d, threshold=%s, edge='%s')"
        #             % (mode, str(mph), mpd, str(threshold), edge))
        # plt.grid()
        plt.show()

