
# coding: utf-8

# In[2]:

import sys
import time
import struct
import numpy as np
import cv2

import matplotlib.pyplot as plt

sys.path.append('../lib')
import ThermalLib as lep

get_ipython().magic('matplotlib inline')

from collections import namedtuple
from skimage.transform import radon


# In[3]:

def showImage(data, segments = None):
    orig_img = lep.thermal_image(data, colorMap = 2, resizeScale = 5, segments=segments)
    plt.imshow(orig_img)
    
def getRadonSinogram(data, segment=[(-1,-1),(0,80),(60,80),(60, 0)], show = False, cold_cut = False):
    rays_no = max(data.shape)
    theta = np.linspace(0., 180., rays_no, endpoint=False)

    if cold_cut:
        data = hist_cut(data)
    
    data = np.uint16(data)
    cv2.normalize(data, data, 0, 256, cv2.NORM_MINMAX)

    mask = lep.inside_points(segment)
    masked = np.ma.masked_array(data, mask).filled(0)
    sinogram = radon(masked.reshape(60,80), theta= theta, circle = False )  
    
    if show:
        plt.imshow(sinogram, cmap=plt.cm.Reds,extent=(0, 180, 0, sinogram.shape[0]), aspect='auto')
        plt.xlabel("Projection angle (deg)")
        plt.ylabel("Projection position (pixels)")
        plt.title("Radon Transform")        
    
    return sinogram


def accumulatedRadon(image_generator, direction = 90, segment=[(-1,-1),(0,80),(60,80),(60, 0)], differential = False, frames = 100, show = False, cold_cut = False):
    
    mask = lep.inside_points(segment)
    rays = []
    
    i = 0
    for data, header in image_generator:
        if cold_cut:
            data = hist_cut(data)
            
        data = np.uint16(data)
        cv2.normalize(data, data, 0, 256, cv2.NORM_MINMAX)
        
        masked = np.ma.masked_array(data, mask).filled(0)
        sinogram = radon(masked.reshape(60,80), theta= [direction], circle = False)
        rays.append(sinogram.reshape(1,sinogram.shape[0])[0])
        i+=1
        if i > frames:
            break

    rows = np.array(rays).T
    
    # difference with previous frame
    if (differential):
        rows = np.diff(rows)
    
    rows = rows[~(rows==0).all(1)]
    rows[rows < 0] = 0
    if show:
        plt.figure(figsize=(20, 6))
        plt.imshow(rows, cmap=plt.cm.Reds, extent=(0, frames, 0, rows.shape[0]), aspect='auto')
        plt.xlabel("Frame number")
        plt.ylabel("Projection position (pixels)")
        plt.title("Radon Transform")        
    # filter zro rows
    return rows

def hist_cut(data):
    hist,bins = np.histogram(data,bins = range(7000, 8500, 20))
    ind = np.argmax(hist)
    cut = bins[ind+1]
    data[data < cut] = cut 
    return data
    


# In[69]:

def radon_flow_lobby7():
    lgen = lep.image_generator("/Users/amin/Desktop/Philips/cam2_corrected.bin", record_len = 9604)
    box2_segment = lep.makeSegment(40,41,20,39)
    rad = accumulatedRadon(lgen, show = True, direction = 90 , segment = box2_segment, differential = True, cold_cut = True, frames = 500)

def filtered_sinogram(data, show=False):
    sinogram = getRadonSinogram(lep.hist_cut(data))
    if show:
        plt.imshow(sinogram, cmap=plt.cm.Reds,extent=(0, 180, 0, sinogram.shape[0]), aspect='auto')
        plt.xlabel("Projection angle (deg)")
        plt.ylabel("Projection position (pixels)")
        plt.title("Radon Transform")
    return sinogram

def radon_pca_distribution(sinogram):
    rays = int(sinogram.shape[1] / 2)
    dists = []
    for ray in range(0,rays):
        dists.append(np.std(np.nonzero(res[:,ray])))
    return np.array(dists).argmax()


# In[19]:

gen = lep.image_generator("/Users/amin/Desktop/Philips/cam2_corrected.bin", record_len = 9604)
box1_segment = lep.makeSegment(37,0,23,40)
box2_segment = lep.makeSegment(37,41,23,39)


# In[20]:

data, header = next(gen2)
showImage(data)


# In[39]:

res = filtered_sinogram(data, show=True)


# In[71]:

radon_pca_distribution(res)


# In[ ]:



