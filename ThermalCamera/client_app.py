from flask import Flask, request, Response
import cv2 
import numpy as np
import time
import ast
import urllib2


app = Flask(__name__)

def makeSegment(x,y,height,width):
    return [(x,y), (x,y+width), (x+height, y+width), (x+height, y) ]

base = "http://lepton.local/"

segment = makeSegment(25,20,15,50)

def capture_thermal():
    f = urllib2.urlopen(base)
    resp = f.read()
    _data = ast.literal_eval(resp.decode())
    
    return np.array(_data)


def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):

    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
    # find indices of all peaks
    dx = x[1:] - x[:-1]
    # handle NaN's
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
    # handle NaN's
    if ind.size and indnan.size:
        # NaN's and values close to NaN's cannot be peaks
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
    # first and last values of x cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
    # remove peaks < minimum peak height
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
    # remove peaks - neighbors < threshold
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
    # detect small peaks closer than minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  # sort ind by peak height
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
                # keep peaks with the same height if kpsh is True
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd)                     & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  # Keep current peak
        # remove the small peaks and sort back the indices by their occurrence
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        _plot(x, mph, mpd, threshold, edge, valley, ax, ind)

    return ind


def axis_radon(data, axis = 0):
    mapped = data.sum(axis)
    
    min_val = mapped.min()
    max_val = mapped.max()
    normal = (mapped - min_val) * 100 / (max_val - min_val)
    return normal.flatten().round()


def thermal_image(data, colorMap = 2, resizeScale = 5, colorRange = [0, 65535], segments = None, peak_points=[]):
    #capSize = (80 * scale, 60 * scale) if (rotate is 0 or rotate is -2) else (60 * scale, 80 * scale)
    data = np.uint16(data)      
    cv2.normalize(data, data,  0,65535, cv2.NORM_MINMAX)
    np.right_shift(data, 8, data)

    img = cv2.applyColorMap(np.uint8(data), colorMap)
    
    img = cv2.resize(img, (0,0), fx=resizeScale, fy=resizeScale) 
    for peak in peak_points:
        cv2.line(img,(peak * resizeScale,0),(peak * resizeScale,30 * resizeScale),(255,255,0),2)
    if segments:
        polys = []
        for segment in segments:
            segment = [(b, a) for a, b in segment]
            poly = np.array(segment) * resizeScale
            cv2.polylines(img, [poly], 1, (255,255,255))

    return img

def gen():
   
    while True:
        time.sleep(0.1)
        data = capture_thermal()

        peaks = []
        sdata = data[segment[0][0]:segment[2][0],segment[0][1]:segment[2][1]]
        if sdata.max() - sdata.min() > 40 : 
            rad = axis_radon(sdata)
            peaks = detect_peaks(rad, edge = 'falling', mpd=5, mph = 70) + segment[0][1]
            
        img = thermal_image(data, segments=[segment], peak_points = peaks)

        ret, jpeg = cv2.imencode('.jpg', img)
        yield (b'--frame\r\n'
            b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tostring() + b'\r\n\r\n')

@app.route('/')
def index():
	return render_template('index.html')
    #return 'Hello world'


@app.route('/lepton_feed')
def lepton_feed():
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')               

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
